package at.willhaben.config

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig
import io.confluent.kafka.serializers.KafkaAvroSerializer
import io.confluent.kafka.serializers.subject.RecordNameStrategy
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.config.KafkaListenerContainerFactory
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer
import org.springframework.kafka.listener.ContainerProperties
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer
import org.springframework.kafka.listener.SeekToCurrentErrorHandler
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2
import org.springframework.kafka.support.serializer.JsonDeserializer
import org.springframework.kafka.support.serializer.JsonSerializer
import org.springframework.util.backoff.FixedBackOff


@Configuration
open class KafkaConfig {
    @Value("\${kafka.bootstrap-servers}")
    private lateinit var bootstrapServers: String
    @Value("\${kafka.group-id:default}")
    private lateinit var groupId: String
    @Value("\${kafka.schema-registry-url}")
    private lateinit var schemaRegistryUrl: String
    @Value("\${kafka.max-poll-interval-ms:60000}")
    private var maxPollIntervalMs: Int = 0
    @Value("\${kafka.max-poll-records:100}")
    private var maxPollRecords: Int = 0

    @Bean
    open fun consumerFactory(): ConsumerFactory<String, SpecificRecord> {
        return DefaultKafkaConsumerFactory(consumerConfig())
    }

    @Primary
    @Bean("producer")
    open fun producer(): KafkaTemplate<String, SpecificRecord> {
        return KafkaTemplate<String, SpecificRecord>(DefaultKafkaProducerFactory<String, SpecificRecord>(producerConfig()))
    }

    @Bean("producerDeadLetter")
    open fun producerDeadLetter(): KafkaTemplate<String, SpecificRecord> {
        return KafkaTemplate<String, SpecificRecord>(DefaultKafkaProducerFactory<String, SpecificRecord>(producerConfig()))
    }


    @Bean
    open fun kafkaListenerContainerFactory(@Qualifier("producerDeadLetter") template: KafkaTemplate<String, SpecificRecord>): KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, SpecificRecord>> {
        val factory = ConcurrentKafkaListenerContainerFactory<String, SpecificRecord>()
        factory.consumerFactory = consumerFactory()
        factory.containerProperties.isAckOnError = false
        factory.containerProperties.ackMode = ContainerProperties.AckMode.RECORD


        factory.setErrorHandler(SeekToCurrentErrorHandler(DeadLetterPublishingRecoverer(template)
        { record, _ -> TopicPartition(record.topic() + ".DLT",-1) }, FixedBackOff(100, 1)))
        return factory
    }

    private fun producerConfig(): Map<String, Any> {
        return mapOf(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServers,
                ProducerConfig.ACKS_CONFIG to "all",
                ProducerConfig.RETRIES_CONFIG to 0,
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG to StringSerializer::class.java,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG to KafkaAvroSerializer::class.java,
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG to schemaRegistryUrl
        )
    }


    private fun consumerConfig(): Map<String, Any> {
        return mapOf(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServers,
                ConsumerConfig.GROUP_ID_CONFIG to groupId,
                ConsumerConfig.AUTO_OFFSET_RESET_CONFIG to "earliest",
                ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG to maxPollIntervalMs,
                ConsumerConfig.MAX_POLL_RECORDS_CONFIG to maxPollRecords,
                ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG to false,
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG to ErrorHandlingDeserializer2::class.java,
                ErrorHandlingDeserializer2.VALUE_DESERIALIZER_CLASS to KafkaAvroDeserializer::class.java,
                KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG to true,
                KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG to schemaRegistryUrl,
                KafkaAvroDeserializerConfig.VALUE_SUBJECT_NAME_STRATEGY to RecordNameStrategy::class.java,
                KafkaAvroDeserializerConfig.AUTO_REGISTER_SCHEMAS to false
        )
    }
}