package at.willhaben.consumer

import at.willhaben.model.Payment
import org.apache.avro.specific.SpecificRecordBase
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import java.util.concurrent.atomic.AtomicInteger

@Component
class EventConsumer {
    private val logger = LoggerFactory.getLogger(javaClass)

    companion object {
        val atomicInteger = AtomicInteger()
    }

    var failureLikelyHood: Int = 10;

    @KafkaListener(topics = ["\${kafka.topic}"])
    private fun handleEvent(@Payload event: SpecificRecordBase,
                            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) partition: Int,
                            @Header(KafkaHeaders.RECEIVED_TOPIC) topic: String,
                            @Header(KafkaHeaders.OFFSET) offset: Int) {
        if (Math.random() < (failureLikelyHood / 100).toDouble()) {
            throw RuntimeException("Some random failure happened!")
        }
        if (event is Payment) {
            logger.info("******* #{} *******", atomicInteger.incrementAndGet())
            logger.info("Consumed event {}", event)
        }
    }
}
