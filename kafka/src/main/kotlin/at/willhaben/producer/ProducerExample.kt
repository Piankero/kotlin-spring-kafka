package at.willhaben.producer

import at.willhaben.model.Payment
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import kotlin.math.pow

@Component
class ProducerExample @Autowired constructor(private val producer: KafkaTemplate<String, SpecificRecord>) : ApplicationListener<ApplicationReadyEvent> {

    @Value("\${kafka.topic}")
    lateinit var topic: String

    var recordsProduces: Int = 10.0.pow(1).toInt();

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun onApplicationEvent(p0: ApplicationReadyEvent) {
        produceValidData();
    }

    fun produceValidData() {
        try {
            var counter = 0;
            for (i in 0 until recordsProduces) {
                val orderId = "id$i"
                val payment = Payment(orderId, i + 1)
                val record: ProducerRecord<String, SpecificRecord> = ProducerRecord(topic, payment.getId().toString(), payment)
                producer.send(record)
                counter++
            }
            producer.flush()
            logger.info("===============================")
            logger.info("{} events produced in topic {}", counter, topic)
            logger.info("===============================")

        } catch (e: Exception) {
            logger.error("Could not produce messages", e);
        }
    }

}