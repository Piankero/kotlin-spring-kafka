# KafkaPrototype

This project should investigate the behavior of kafka and kotlin in case of errors. All outcomes and tests can be seen
in the [FINDINGS.md](FINDINGS.md).

## Setup

I used a docker-compose setup from [confluent](https://docs.confluent.io/current/quickstart/ce-docker-quickstart.html?utm_medium=sem&utm_source=google&utm_campaign=ch.sem_br.brand_tp.prs_tgt.confluent-brand_mt.xct_rgn.emea_lng.eng_dv.all&utm_term=confluent%20kafka%20docker&creative=&device=c&placement=&gclid=Cj0KCQiAyKrxBRDHARIsAKCzn8yT2dClsfB1W5GlQASgiz3uP9Geta-5X2zcEhauLVjACBY7aDedfY8aAtrcEALw_wcB)

```
git clone https://github.com/confluentinc/examples
cd examples
git checkout 5.4.0-post
cd cp-all-in-one
docker-compose up -d --build
```

On the frontend localhost:9021 I created a new topic called `transactions` which has exactly two partitions.
The schema for this topic can be seen in the module `model` in this project. The resources folder contains the
avro schema.

## Variants

Currently I implemented two variant. Which can be seen in different branches:

### Variant 1 (master)

This variant uses a SkipToCurrentErrorHandler which also uses a Recoverer to a DeadLetterQueue.

### Variant 2 (Acknowledge)

This variant uses a custom acknowledge strategy. There is a problem though with endless loops if you always
no-acknowledge certain records.