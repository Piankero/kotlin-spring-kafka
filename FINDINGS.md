# Some insights into Kafka related errors

## Pushing nonsense into the topic via cmd line

```
kafka-console-producer.sh --broker-list localhost:9092 --topic transactions
>myNonsense
```

If the message is not in a correct avro schema, Spring will reject and skip it.

```
org.springframework.kafka.listener.ListenerExecutionFailedException: Listener failed; nested exception is org.springframework.kafka.support.serializer.DeserializationException: failed to deserialize; nested exception is org.apache.kafka.common.errors.SerializationException: Error deserializing Avro message for id -1
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.decorateException(KafkaMessageListenerContainer.java:1745) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.invokeErrorHandler(KafkaMessageListenerContainer.java:1734) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.doInvokeRecordListener(KafkaMessageListenerContainer.java:1647) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.doInvokeWithRecords(KafkaMessageListenerContainer.java:1577) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.invokeRecordListener(KafkaMessageListenerContainer.java:1485) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.invokeListener(KafkaMessageListenerContainer.java:1235) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.pollAndInvoke(KafkaMessageListenerContainer.java:985) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.run(KafkaMessageListenerContainer.java:905) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at java.base/java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:515) ~[na:na]
	at java.base/java.util.concurrent.FutureTask.run$$$capture(FutureTask.java:264) ~[na:na]
	at java.base/java.util.concurrent.FutureTask.run(FutureTask.java) ~[na:na]
	at java.base/java.lang.Thread.run(Thread.java:834) ~[na:na]
Caused by: org.springframework.kafka.support.serializer.DeserializationException: failed to deserialize; nested exception is org.apache.kafka.common.errors.SerializationException: Error deserializing Avro message for id -1
	at org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2.deserializationException(ErrorHandlingDeserializer2.java:228) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2.deserialize(ErrorHandlingDeserializer2.java:203) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.apache.kafka.clients.consumer.internals.Fetcher.parseRecord(Fetcher.java:1268) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher.access$3600(Fetcher.java:124) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher$PartitionRecords.fetchRecords(Fetcher.java:1492) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher$PartitionRecords.access$1600(Fetcher.java:1332) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher.fetchRecords(Fetcher.java:645) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher.fetchedRecords(Fetcher.java:606) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.KafkaConsumer.pollForFetches(KafkaConsumer.java:1294) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.KafkaConsumer.poll(KafkaConsumer.java:1225) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.KafkaConsumer.poll(KafkaConsumer.java:1201) ~[kafka-clients-2.3.1.jar:na]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.doPoll(KafkaMessageListenerContainer.java:1012) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.pollAndInvoke(KafkaMessageListenerContainer.java:968) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	... 5 common frames omitted
Caused by: org.apache.kafka.common.errors.SerializationException: Error deserializing Avro message for id -1
Caused by: org.apache.kafka.common.errors.SerializationException: Unknown magic byte!
```

## Pushing something incompatible with the current schema via cmd line

```
kafka-avro-console-producer \                        
         --broker-list localhost:9092 --topic transactions \
         --property value.schema='{"type":"record","name":"at.willhaben.model.Payment","fields":[{"name":"f1","type":"string"}]}'
{"f1": "value1"}
```

Kafka correctly rejects the invalid Schema

```
org.apache.kafka.common.errors.SerializationException: Error registering Avro schema: {"type":"record","name":"myrecord","fields":[{"name":"f1","type":"string"}]}
Caused by: io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException: Schema being registered is incompatible with an earlier schema; error code: 409
	at io.confluent.kafka.schemaregistry.client.rest.RestService.sendHttpRequest(RestService.java:182)
	at io.confluent.kafka.schemaregistry.client.rest.RestService.httpRequest(RestService.java:203)
	at io.confluent.kafka.schemaregistry.client.rest.RestService.registerSchema(RestService.java:292)
	at io.confluent.kafka.schemaregistry.client.rest.RestService.registerSchema(RestService.java:284)
	at io.confluent.kafka.schemaregistry.client.rest.RestService.registerSchema(RestService.java:279)
	at io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient.registerAndGetId(CachedSchemaRegistryClient.java:61)
	at io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient.register(CachedSchemaRegistryClient.java:93)
	at io.confluent.kafka.serializers.AbstractKafkaAvroSerializer.serializeImpl(AbstractKafkaAvroSerializer.java:74)
	at io.confluent.kafka.formatter.AvroMessageReader.readMessage(AvroMessageReader.java:158)
	at kafka.tools.ConsoleProducer$.main(ConsoleProducer.scala:58)
	at kafka.tools.ConsoleProducer.main(ConsoleProducer.scala)

```

## Pushing something which does not fulfill the Schema definition via cmd line

```
kafka-avro-console-producer \                                                                                     
  --broker-list localhost:9092 --topic transactions \
  --property value.schema='{"type":"record","name":"at.willhaben.model.Payment","fields":[{"name":"id","type":"string"}, {"name":"sequenceNumber","type":"int"}]}'
{"id":"id111","sequenceNumber":"1"} # This is a string and not a number
```

Avro rejects any message like this:

```
org.apache.kafka.common.errors.SerializationException: Error deserializing json {"id":"id111","sequenceNumber":"1"} to Avro of schema {"type":"record","name":"Payment","namespace":"at.willhaben.model","fields":[{"name":"id","type":"string"},{"name":"sequenceNumber","type":"int"}]}
Caused by: org.apache.avro.AvroTypeException: Expected int. Got VALUE_STRING
	at org.apache.avro.io.JsonDecoder.error(JsonDecoder.java:698)
	at org.apache.avro.io.JsonDecoder.readInt(JsonDecoder.java:172)
	at org.apache.avro.io.ValidatingDecoder.readInt(ValidatingDecoder.java:83)
	at org.apache.avro.generic.GenericDatumReader.readInt(GenericDatumReader.java:511)
	at org.apache.avro.generic.GenericDatumReader.readWithoutConversion(GenericDatumReader.java:182)
	at org.apache.avro.generic.GenericDatumReader.read(GenericDatumReader.java:152)
	at org.apache.avro.generic.GenericDatumReader.readField(GenericDatumReader.java:240)
	at org.apache.avro.generic.GenericDatumReader.readRecord(GenericDatumReader.java:230)
	at org.apache.avro.generic.GenericDatumReader.readWithoutConversion(GenericDatumReader.java:174)
	at org.apache.avro.generic.GenericDatumReader.read(GenericDatumReader.java:152)
	at org.apache.avro.generic.GenericDatumReader.read(GenericDatumReader.java:144)
	at io.confluent.kafka.formatter.AvroMessageReader.jsonToAvro(AvroMessageReader.java:190)
	at io.confluent.kafka.formatter.AvroMessageReader.readMessage(AvroMessageReader.java:157)
	at kafka.tools.ConsoleProducer$.main(ConsoleProducer.scala:58)
	at kafka.tools.ConsoleProducer.main(ConsoleProducer.scala)
```

## Pushing a different SpecificRecord into Kafka via cmdline

```
kafka-avro-console-producer \                                                                                   
  --broker-list localhost:9092 --topic transactions \
  --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"id","type":"string"}, {"name":"sequenceNumber","type":"int"}]}' ## Note the "myrecord" instead of at.willhaben.model.Payment
## This has to be manually typed in
{"id":"id3","sequenceNumber":3}
```

Kafka successfully skips the entry.

```
org.springframework.kafka.listener.ListenerExecutionFailedException: Listener failed; nested exception is org.springframework.kafka.support.serializer.DeserializationException: failed to deserialize; nested exception is org.apache.kafka.common.errors.SerializationException: Error deserializing Avro message for id 2
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.decorateException(KafkaMessageListenerContainer.java:1745) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.invokeErrorHandler(KafkaMessageListenerContainer.java:1734) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.doInvokeRecordListener(KafkaMessageListenerContainer.java:1647) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.doInvokeWithRecords(KafkaMessageListenerContainer.java:1577) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.invokeRecordListener(KafkaMessageListenerContainer.java:1485) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.invokeListener(KafkaMessageListenerContainer.java:1235) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.pollAndInvoke(KafkaMessageListenerContainer.java:985) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.run(KafkaMessageListenerContainer.java:905) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at java.base/java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:515) ~[na:na]
	at java.base/java.util.concurrent.FutureTask.run$$$capture(FutureTask.java:264) ~[na:na]
	at java.base/java.util.concurrent.FutureTask.run(FutureTask.java) ~[na:na]
	at java.base/java.lang.Thread.run(Thread.java:834) ~[na:na]
Caused by: org.springframework.kafka.support.serializer.DeserializationException: failed to deserialize; nested exception is org.apache.kafka.common.errors.SerializationException: Error deserializing Avro message for id 2
	at org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2.deserializationException(ErrorHandlingDeserializer2.java:228) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2.deserialize(ErrorHandlingDeserializer2.java:203) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.apache.kafka.clients.consumer.internals.Fetcher.parseRecord(Fetcher.java:1268) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher.access$3600(Fetcher.java:124) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher$PartitionRecords.fetchRecords(Fetcher.java:1492) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher$PartitionRecords.access$1600(Fetcher.java:1332) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher.fetchRecords(Fetcher.java:645) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.internals.Fetcher.fetchedRecords(Fetcher.java:606) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.KafkaConsumer.pollForFetches(KafkaConsumer.java:1294) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.KafkaConsumer.poll(KafkaConsumer.java:1225) ~[kafka-clients-2.3.1.jar:na]
	at org.apache.kafka.clients.consumer.KafkaConsumer.poll(KafkaConsumer.java:1201) ~[kafka-clients-2.3.1.jar:na]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.doPoll(KafkaMessageListenerContainer.java:1012) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	at org.springframework.kafka.listener.KafkaMessageListenerContainer$ListenerConsumer.pollAndInvoke(KafkaMessageListenerContainer.java:968) ~[spring-kafka-2.3.5.RELEASE.jar:2.3.5.RELEASE]
	... 5 common frames omitted
Caused by: org.apache.kafka.common.errors.SerializationException: Error deserializing Avro message for id 2
Caused by: org.apache.kafka.common.errors.SerializationException: Could not find class myrecord specified in writer's schema whilst finding reader's schema for a SpecificRecord.
```

## Randomly throwing errors on certain records (database failure simulation)

I used the following code to emulate this behavior:

```java
@KafkaListener(topics = ["\${kafka.topic}"])
private fun handleEvent(@Payload event: SpecificRecordBase) {
    var d = Math.random();
    if (d < 0.5) {
        throw RuntimeException("Oops, database connection failure")
    }
    if (event is Payment) {
        logger.info("Consumed {} events", atomicInteger.incrementAndGet())
        logger.info("Consumed event {}", event)
    }
}
```

On my machine I received the expected runtime errors and ended with the following message (sent 10 messages into queue):

```
Consumed 6 events
Consumed event {"id": "myId", "sequenceNumber": 20}
```

This means that in case of an exception Kafka **does not** retry.

## Randomly throwing errors on certain records with Acknowledge (database failure simulation)

I changed the Acknowledge mode like the following:

```java
@KafkaListener(topics = ["\${kafka.topic}"])
private fun handleEvent(@Payload event: SpecificRecordBase, ack: Acknowledgment) {
    var d = Math.random();
    if (d < 0.5) {
        throw RuntimeException("Oops, database connection failure")
    }
    if (event is Payment) {
        logger.info("Consumed {} events", atomicInteger.incrementAndGet())
        logger.info("Consumed event {}", event)
    }
    ack.acknowledge()
}
```

No changes to the previous example.

## Randomly throwing errors with SeekToCurrentErrorHandler

I changed the ContainerFactory with the following code
```java
    @Bean
    open fun kafkaListenerContainerFactory(): KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, SpecificRecord>> {
    val factory = ConcurrentKafkaListenerContainerFactory<String, SpecificRecord>()
    factory.consumerFactory = consumerFactory()
    factory.containerProperties.isAckOnError = false
    factory.containerProperties.ackMode = ContainerProperties.AckMode.RECORD
    factory.setErrorHandler(SeekToCurrentErrorHandler())
    return factory
    }
```

Now Kafka retries and the SeekToCurrentErrorHandler is very configurable (e.g., which errors it should not retry on.)

https://docs.spring.io/spring-kafka/reference/html/#seek-to-current

