kafka-delete-records.sh --bootstrap-server localhost:9092 --offset-json-file delete-records.json

kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic transactions --from-beginning

kafka-console-producer.sh --broker-list localhost:9092 --topic transactions

kafka-avro-console-producer \
  --broker-list localhost:9092 --topic transactions \
  --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"id","type":"string"}, {"name":"sequenceNumber","type":"int"}]}'

kafka-avro-console-consumer --topic transactions \
             --bootstrap-server localhost:9092 \
             --from-beginning